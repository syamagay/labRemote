# add executables
file(GLOB tools [a-zA-Z]*.cpp)

foreach(target ${tools})
  get_filename_component(execname ${target} NAME_WE)
  get_filename_component(srcfile ${target} NAME)

  add_executable(${execname})
  target_sources(${execname} PRIVATE ${srcfile})
  target_link_libraries(${execname} PRIVATE Com PS Meter Arduino Load DevCom Utils InfluxDB)

  if( ${libScope_FOUND} )
    target_link_libraries(${execname} PRIVATE Scope)
    target_compile_definitions(${execname} PRIVATE SCOPE=1)
  endif()

endforeach()
