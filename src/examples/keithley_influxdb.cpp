#include <iostream>
#include <getopt.h>
#include <string>
#include <vector>
#include <signal.h>
#include <algorithm>

#include "Logger.h"
#include "Keithley24XX.h"
#include "influxdb.h"

// Parameters to initialize the influxDB object
#define host "127.0.0.1"
#define influxPort 8086
#define dbname "dcsDB"

std::string usbPort("/dev/ttyUSB4");  // CL parameter "--port"
int address = 24;                     // CL parameter "--addr"
int nUploads = 0;                     // CL parameter "toInfluxDB"

loglevel_e loglevel = logINFO;

void usage(char *argv[]) {
  std::cerr << "Usage: " << argv[0] << " [options] command [parameters]" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "List of possible COMMAND:" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "  set-current [I]   Set the value of current [A]" << std::endl;
  std::cerr << "  set-voltage [V]   Set the value of voltage [V]" << std::endl;
  std::cerr << "  get-current       Get reading of current [A]" << std::endl;
  std::cerr << "  get-voltage       Get reading of voltage [V]" << std::endl;
  std::cerr << std::endl;
  std::cerr << "  to-influxdb [N]   Upload N 'I' and 'V' measurements (separated 1 s) to InfluxDB" << std::endl;
  std::cerr << "                    Do not specify N to start an infinite loop" << std::endl;
  std::cerr << std::endl;
  std::cerr << "  power-on [V I]    Power ON meter, optionally setting voltage to V in Volts and current to I in Ampere." << std::endl;
  std::cerr << "  power-off         Power OFF meter" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << " -p, --port        Set serial port to which the meter is connected to (default: " << usbPort << ")" << std::endl;
  std::cerr << " -g, --gpib        Set GPIB address to which the meter is connected to (default: " << address << ")" << std::endl;
  std::cerr << "" << std::endl;
}

void signalHandler(int signal){
  std::cout << "Interrupt signal received. Exiting normally." << std::endl;
  exit(EXIT_SUCCESS);
}

int main(int argc, char*argv[]) {

  if (argc < 2) {
    usage(argv);
    return 1;
  }

  //Parse command-line
  int c;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"port",     required_argument, 0,  'p' },
      {"gpib",     required_argument, 0,  'g' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "p:g:", long_options, &option_index);
    if (c == -1)
      break;

    switch (c) {
    case 'p':
      usbPort = optarg;
      break;
    case 'g':
      address = atoi(optarg);
      break;
    default:
      std::cerr << "Invalid option supplied. Aborting." << std::endl;
      std::cerr << std::endl;
      usage(argv);
    }
  }
  std::string command;
  std::vector<std::string> params;
  if (optind < argc) {
    command = argv[optind++];
    std::transform(command.begin(), command.end(), command.begin(), ::tolower);
    while (optind < argc) {
      std::string p(argv[optind++]);
      std::transform(p.begin(), p.end(), p.begin(), ::tolower);
      params.push_back(p);
    }
  }

  Keithley24XX meter(usbPort, address);
  


  //Now interpret "command"
  if (command == "get-current") {
    logger(logINFO) << meter.sense(KeithleyMode::CURRENT);
  } 
  else if (command == "get-voltage"){
    logger(logINFO) << meter.sense(KeithleyMode::VOLTAGE);
  } 
  else if (command == "set-current" | command == "set-voltage"){
    if (meter.isOn())
    {
      if (params.size() > 0) 
      {
        if (command == "set-current"){
          meter.setSource(KeithleyMode::CURRENT, atof(params[0].c_str()), atof(params[0].c_str()));
        }
        else {
          meter.setSource(KeithleyMode::VOLTAGE, atof(params[0].c_str()), atof(params[0].c_str()));
        }
        logger(logINFO) << "done";
      }
      else{
        logger(logERROR) << "No params specified to the " << command << "command";
        usage(argv);
	      return 1;
      }
    }
    else{
      logger(logERROR) << "The DCS is not turned on.";
      usage(argv);
	    return 1;
    }
  }
  else if (command == "power-off"){
    logger(logINFO) << "Powering OFF.";
    meter.turnOff();
  }
  else if (command == "power-on") 
  {    
    if (meter.isOn()){
      logger(logINFO) << "Already powered ON.";
    }
    else
    {
      logger(logINFO) << "Powering ON.";
      meter.init();
      if (params.size() > 0) 
      {
        if (params.size() != 2) 
        {
	        std::cerr << "Invalid number of parameters to power-on command." << std::endl;
	        std::cerr << std::endl;
	        usage(argv);
	        return 1;
        }
        meter.setSense(KeithleyMode::VOLTAGE, atof(params[0].c_str()), atof(params[0].c_str()));
        meter.setSource(KeithleyMode::CURRENT, atof(params[1].c_str()), atof(params[1].c_str()));
      }
      meter.turnOn();
    }
  }
  else if (command == "to-influxdb")
  {
    nUploads = (params.size() > 0) ? atoi(params[0].c_str()) : -1;
    if (meter.isOn())
    {
      signal(SIGINT, signalHandler);

      InfluxHandler* ih;
      ih=new InfluxHandler(host,influxPort,dbname);
      ih->meas="Keithley24XX_" + std::to_string(address) + '_';

      std::string field_v_name="1_HV_V";
      std::string field_i_name="1_HV_I";
      std::string field_username=getlogin();

      std::string field_vu_name="1_HV_V_unit";
      std::string field_iu_name="1_HV_I_unit";

      for (unsigned i = 0; i < nUploads; i++) 
      {
        float voltage=std::stod(meter.sense(KeithleyMode::VOLTAGE));
        float current=std::stod(meter.sense(KeithleyMode::CURRENT));

        ih->setData(field_i_name,current,field_iu_name,"[A]");
        ih->setData(field_v_name,voltage,field_vu_name,"[V]");
        ih->setData("user",getlogin());
        ih->uploadToInfluxDB();
        logger(logINFO) << "Voltage = "<< voltage  << " [V], Current=" << meter.sense(KeithleyMode::CURRENT) << " [A]";
        sleep(0);
      }
    }
    else{
      logger(logERROR) << "The DCS is not turned on.";
      usage(argv);
      return 1;
    }
  }
  else{
    std::cerr << "Required command argument missing." << std::endl;
    std::cerr << std::endl;
    usage(argv);
    return 1;
  }

  return 0;
}

