#include "influxdb.h"
#include <iostream>
#include <algorithm>
#include <math.h>

unsigned GetDigit(unsigned num){
  return log10(num)+1;
}

InfluxHandler::InfluxHandler(){
  si=new influxdb_cpp::server_info("127.0.0.1",8086,"test");
}
InfluxHandler::InfluxHandler(std::string host, int port, std::string dbname){
  si=new influxdb_cpp::server_info(host,port,dbname);
  this->createDB(dbname);
}

void InfluxHandler::setDBStatus(std::string host,int port , std::string dbname){
  si->host_=host;
  si->port_=port;
  si->db_=dbname;
  this->createDB(dbname);
}

void InfluxHandler::createDB(std::string dbname){
  std::string resp;
  influxdb_cpp::create_db(resp,dbname,*si);
}

bool InfluxHandler::checkDB(std::string dbname){
  std::string resp;
  influxdb_cpp::get_db_list(resp,*si);
  return true;
}

void InfluxHandler::setData(std::string tname, std::string tvalue){
  setData("", 0, tname, tvalue);
}
void InfluxHandler::setData(std::string fname, double fvalue){
  setData(fname, fvalue, "", "");
}

void InfluxHandler::setData(std::string fname, double fvalue, std::string tname, std::string tvalue/*, time_t time*/){

  if (fname != "")
  {
    field_name.push_back(fname);
    field_value.push_back(fvalue);
  }
  if (tname != "")
  {
    tag_name.push_back(tname);
    tag_value.push_back(tvalue);
  }


  /*
  long int t= time;
  if(GetDigit(time)==10){
    t=t*1E9;
  }
  else if(GetDigit(time)!=19){
    std::cout << "time : " << time << "is not either unixtime or nano-unixtime!" << std::endl;
    std::exit(0);
  }
  uploadtime=t;
  
  this->uploadToInfluxDB();
  */
}
void InfluxHandler::uploadToInfluxDB(time_t time){
  if(time!=0){
    long int t= time;
    if(GetDigit(time)==10){
      t=t*1E9;
    }
    else if(GetDigit(time)!=19){
      std::cout << "time : " << time << "is not either unixtime or nano-unixtime!" << std::endl;
      std::exit(0);
    }
    uploadtime=t;
  }
  std::string resp;
  influxdb_cpp::builder* builder=new influxdb_cpp::builder();
  influxdb_cpp::detail::tag_caller& measurement=builder->meas(meas);
  for(int i=0; i<tag_name.size();i++){
    measurement.tag(tag_name[i], tag_value[i]);
  }
  influxdb_cpp::detail::field_caller* field_caller;
  for(int i=0; i<field_name.size();i++){
    if (i==0){
      field_caller=&(measurement.field(field_name[i], field_value[i]));
    }
    else{
      field_caller->field(field_name[i], field_value[i]);
    }
  }
  field_caller->post_http(*si, &resp);
  tag_name.clear();
  tag_value.clear();
  field_name.clear();
  field_value.clear();

  //  std::cout << resp << std::endl;
}
/*
InfluxHandler* init_Influx(bool use_influx, std::string meas_name){
  if(use_influx){
    std::string host="127.0.0.1";
    int port=8086;
    std::string dbname="test";
    InfluxHandler* ih=new InfluxHandler(host,port,dbname); 
    ih->use_influxDB=true;
    ih->meas=meas_name;
    //ih->setDBStatus(host,port,dbname);
    return ih;
  }
  else{
    ih->use_influxDB=false;
  }
}
*/

double string_to_double(std::string str){
  int EPos=str.find("E");
  if(EPos==std::string::npos){
    return std::stod(str);
  }
}

